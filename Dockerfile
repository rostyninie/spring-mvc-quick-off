FROM alpine/git as repo
WORKDIR /app
RUN git clone https://gitlab.com/rostyninie/spring-mvc-quick-off.git

FROM maven:3.5.2-jdk-8-alpine as build
WORKDIR /app
COPY --from=repo /app/spring-mvc-quick-off /app
RUN mvn package -DskipTests

FROM tomcat
COPY --from=build /app/target/app.war /usr/local/tomcat/webapps/
COPY --from=build /app/target/app /usr/local/tomcat/webapps/ROOT
expose 8080