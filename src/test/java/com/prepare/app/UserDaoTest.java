package com.prepare.app;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.prepare.app.dao.UserDao;
import com.prepare.app.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations="/META-INF/root-test-context.xml")
public class UserDaoTest {
	
	@Autowired
	UserDao userDao;
	
	private User user;
	
	@Before
	public void initUse() {
		user =new User();
		user.setUserName("Foching");
		user.setPassword("Rost");
		user.setEmail("tat");
	}
	
	@Test
	public void saveUser() {
		user =userDao.persist(user);
		assertNotNull(Long.valueOf(user.getId()));
	}

}
