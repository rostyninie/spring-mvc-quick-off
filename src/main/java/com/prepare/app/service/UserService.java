package com.prepare.app.service;

import java.util.List;

import com.prepare.app.entities.Employee;
import com.prepare.app.entities.User;

public interface UserService {

	Employee save();
	
	User saveUser(User user) ;
	
	List<User> getAll();
	
	User getById(long id);
	
	User saveOrUpdate(User user);
	
	void deleteUser(long id);

}