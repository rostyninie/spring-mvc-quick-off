package com.prepare.app.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
public class TrackerUpdatePropertyAspect {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrackerUpdatePropertyAspect.class);
	
	@Before("execution(void set*(..))")
	public void trackChange() {
		LOGGER.info("Property has changer");
	}

}
