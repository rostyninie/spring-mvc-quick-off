package com.prepare.app.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import javax.persistence.GenerationType;

@Table(name="t_user")
@Entity
public class User extends AbstractIdentifiableEntity implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7756846616260826311L;


	
	@Column(name = "user_name")
	private String userName;
	
	private String password;
	
	private String email;

	
	@ManyToMany(fetch = FetchType.EAGER, targetEntity = Role.class, cascade =  CascadeType.ALL)
	@JoinTable(name = "t_users_Roles",
	joinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name ="Fk_user_role")),
	inverseJoinColumns = @JoinColumn(name = "role_id") , foreignKey = @ForeignKey(name ="Fk_role_user"))
	private Set<Role> roles;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, targetEntity = Employee.class, cascade =  CascadeType.ALL)
	private Set<Employee> employees;



	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
