package com.prepare.app.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="t_role")
public class Role extends AbstractIdentifiableEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -154564426913976646L;
	
	
	@Column(name="role_name")
	private String roleName;
	
	@ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER, targetEntity =  User.class, cascade= CascadeType.ALL)
	private Set<User> users;


	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
	

}
