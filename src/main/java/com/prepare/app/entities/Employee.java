package com.prepare.app.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_employee")
public class Employee extends AbstractIdentifiableEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8781675539395325188L;
	
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name ="last_name")
	private String lastName;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade =  CascadeType.ALL)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name ="FK_employee_user"))
	private User user;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	

}
