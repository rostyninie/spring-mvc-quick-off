package com.prepare.app.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prepare.app.entities.Employee;
import com.prepare.app.entities.User;
import com.prepare.app.service.UserService;

/**
 * Handles requests for the application home page.
 */
@RestController
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
   @Autowired
   UserService userService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value="/users", method =  RequestMethod.GET)
	public Employee getUser() {
		logger.info("Test log");
		return userService.save();
	}
	
	@RequestMapping(value="/", method =  RequestMethod.POST)
	public User getUser(@RequestBody User user) {
		logger.info("Test log");
		user.setEmail("rostyJoha@gmail.com");
		return userService.saveUser(user);
	}
	
	@RequestMapping(value="/", method =  RequestMethod.GET)
	public List<User> getAll(){
		return userService.getAll();
	}
	
	@RequestMapping(value="/user", method =  RequestMethod.GET)
	public User getById(@RequestParam("id") long id){
		return userService.getById(id);
	}
	
	@RequestMapping(value="/", method =  RequestMethod.PUT)
	public User update(@RequestBody User user){
		return userService.saveOrUpdate(user);
	}
	
	@RequestMapping(value="/", method =  RequestMethod.DELETE)
	public void delete(@RequestParam("id") long id){
		 userService.deleteUser(id);
	}
	
}
