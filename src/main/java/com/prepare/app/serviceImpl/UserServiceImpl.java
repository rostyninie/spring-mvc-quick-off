package com.prepare.app.serviceImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prepare.app.dao.EmployeeDao;
import com.prepare.app.dao.RoleDao;
import com.prepare.app.dao.UserDao;
import com.prepare.app.entities.Employee;
import com.prepare.app.entities.Role;
import com.prepare.app.entities.User;
import com.prepare.app.service.UserService;

@Service
public class UserServiceImpl extends AbstractTransactionalService implements UserService {

	@Autowired
	UserDao  userDao;
	
	@Autowired
	RoleDao  roleDao;
	
	@Autowired
	EmployeeDao employeeDao;
	/* (non-Javadoc)
	 * @see com.prepare.app.serviceImpl.UserService#save()
	 */
	@Override
	public Employee save() {
		
		Employee emp = new Employee();
		emp.setFirstName("Foching");
		emp.setLastName("Reine");
		User user = new User();
		user.setUserName("Johana");
		user.setPassword("Siakwoua");
		user.setEmail("Leslie");
		Role role = new Role();
		role.setRoleName("Admin");
		Set<Role> roles =  new HashSet<Role>();
		roles.add(role);
		user.setRoles(roles);
		emp.setUser(user);
		emp =  employeeDao.persist(emp);
		return emp;
	}
	
	public User saveUser(User user) {
		return userDao.persist(user);
	}
	
	public List<User> getAll(){
		return userDao.retrieveAll();
	}
	
	public User getById(long id) {
		return userDao.retrieveById(id);
	}
	
	public User saveOrUpdate(User user) {
		return userDao.saveOrUpdate(user);
	}
	
	public void deleteUser(long id) {
		userDao.remove(userDao.retrieveById(id));
	}
	
	
	
}
