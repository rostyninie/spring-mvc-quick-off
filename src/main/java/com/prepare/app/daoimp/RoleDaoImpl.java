package com.prepare.app.daoimp;

import org.springframework.stereotype.Repository;

import com.prepare.app.dao.RoleDao;
import com.prepare.app.entities.Role;

@Repository
public class RoleDaoImpl extends DaoImpl<Role> implements RoleDao {

}
