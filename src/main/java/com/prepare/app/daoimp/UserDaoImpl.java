package com.prepare.app.daoimp;

import org.springframework.stereotype.Repository;

import com.prepare.app.dao.UserDao;
import com.prepare.app.entities.User;

@Repository
public class UserDaoImpl extends DaoImpl<User> implements UserDao {

}
