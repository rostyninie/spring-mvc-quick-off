package com.prepare.app.daoimp;

import org.springframework.stereotype.Repository;

import com.prepare.app.dao.EmployeeDao;
import com.prepare.app.entities.Employee;

@Repository
public class EmployeeDaoImpl extends DaoImpl<Employee> implements EmployeeDao {

}
