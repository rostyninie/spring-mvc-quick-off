package com.prepare.app.daoimp;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prepare.app.dao.Dao;
import com.prepare.app.entities.AbstractIdentifiableEntity;

public abstract class DaoImpl<T extends AbstractIdentifiableEntity> implements Dao<T> {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(DaoImpl.class);
	
	public static final String BASE_QUERY = "select t from ";
	
	@PersistenceContext
	private EntityManager entityManager;
	
	protected Class<T> entityType;

	
	
	@SuppressWarnings("unchecked")
	public DaoImpl() {
		// r�cup�re la super class direct de type g�n�rique exp : Dao<User>
		Type t = getClass().getGenericSuperclass();
		// r�cup�r� son type parametr�
		ParameterizedType tp = (ParameterizedType)t;
		
		entityType = (Class<T>)tp.getActualTypeArguments()[0];
	}

	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#getEntityManager()
	 */
	@Override
	public EntityManager getEntityManager() {
		return this.entityManager;
	}
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#findById(long)
	 */
	@Override
	public Optional<T> findById(long id) {
		
		return Optional.ofNullable(getEntityManager().find(entityType, id));
	}
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#retrieveById(long)
	 */
	@Override
	public T retrieveById(long id) {
		Optional<T> elt = findById(id);
		if(elt.isPresent()) {
			return elt.get();
		}
		throw new  EntityNotFoundException(entityType.getName());
	}
	
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#persist(T)
	 */
	@Override
	public T persist(T elt) {
		getEntityManager().persist(elt);
		return this.retrieveById(elt.getId());
	}
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#saveOrUpdate(T)
	 */
	@Override
	public T saveOrUpdate(T elt) {
		return this.getEntityManager().merge(elt);
	}
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#retrieveAll()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<T> retrieveAll(){
		StringBuilder query = new StringBuilder();
		query.append(BASE_QUERY);
		query.append(entityType.getSimpleName());
		query.append(" t");
		return this.getEntityManager().createQuery(query.toString()).getResultList();
	}
	
	/* (non-Javadoc)
	 * @see com.prepare.app.daoimp.Dao#remove(T)
	 */
	@Override
	public void remove(T elt) {
		this.getEntityManager().remove(elt);
	}

}
