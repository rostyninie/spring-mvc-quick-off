package com.prepare.app.dao;

import com.prepare.app.entities.User;

public interface UserDao extends Dao<User> {

}
