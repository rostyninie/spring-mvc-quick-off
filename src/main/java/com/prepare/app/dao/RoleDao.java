package com.prepare.app.dao;

import com.prepare.app.entities.Role;

public interface RoleDao extends Dao<Role> {

}
