package com.prepare.app.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.prepare.app.entities.AbstractIdentifiableEntity;

public interface Dao<T extends AbstractIdentifiableEntity> {

	EntityManager getEntityManager();

	Optional<T> findById(long id);

	T retrieveById(long id);

	T persist(T elt);

	T saveOrUpdate(T elt);

	List<T> retrieveAll();

	void remove(T elt);

}