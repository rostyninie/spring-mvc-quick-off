package com.prepare.app.dao;

import com.prepare.app.entities.Employee;

public interface EmployeeDao extends Dao<Employee> {

}
